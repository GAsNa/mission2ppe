package android.gasna.mission2ppe.ui;

public class Interet {
    private int identifiant;
    private String elemPatri;
    private String elemPrinci;

    public int getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(int identifiant) {
        this.identifiant = identifiant;
    }

    public String getElemPatri() {
        return elemPatri;
    }

    public void setElemPatri(String elemPatri) {
        this.elemPatri = elemPatri;
    }

    public String getElemPrinci() {
        return elemPrinci;
    }

    public void setElemPrinci(String elemPrinci) {
        this.elemPrinci = elemPrinci;
    }

    @Override
    public String toString() {
        return "Interet{" +
                "identifiant=" + identifiant +
                ", elemPatri='" + elemPatri + '\'' +
                ", elemPrinci='" + elemPrinci + '\'' +
                '}';
    }
}
