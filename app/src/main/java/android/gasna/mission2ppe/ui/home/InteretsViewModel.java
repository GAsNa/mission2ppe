package android.gasna.mission2ppe.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class InteretsViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public InteretsViewModel() {
    }

    public LiveData<String> getText() {
        return mText;
    }
}