package android.gasna.mission2ppe.ui;

import android.content.Intent;
import android.gasna.mission2ppe.R;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class StatsInterets extends AppCompatActivity {

    private String elemPatri;
    private String elemPrinci;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats_interets);

        Intent intent = getIntent();

        //Si l'intent possède des informations
        if (intent != null) {

            elemPatri = intent.getStringExtra("elemPatri");
            elemPrinci = intent.getStringExtra("elemPrinci");

            TextView tvname = (TextView) findViewById(R.id.tvElemPatri);
            tvname.setText("Nom : " + elemPatri);

            TextView tvtypes = (TextView) findViewById(R.id.tvElemPrinci);
            tvtypes.setText("Description : " + elemPrinci);

        }

    }
}
