package android.gasna.mission2ppe.ui;

import android.content.Context;
import android.gasna.mission2ppe.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class InteretAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Interet> interets;

    public InteretAdapter(Context context, ArrayList<Interet> interets) {
        this.context = context;
        this.interets = interets;
    }

    @Override
    public int getCount() {
        return interets.size();
    }

    @Override
    public Interet getItem(int position) {
        return interets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.interets_row, parent, false);
        }

        Interet currentInteret = interets.get(position);

        final TextView interetElemPatri = (TextView) convertView.findViewById(R.id.interets_noms);
        interetElemPatri.setText(currentInteret.getElemPatri());

        return convertView;
    }

    public ArrayList<Interet> getInterets() {
        return interets;
    }
}
