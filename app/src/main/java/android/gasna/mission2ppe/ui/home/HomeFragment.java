package android.gasna.mission2ppe.ui.home;

import android.annotation.TargetApi;
import android.gasna.mission2ppe.MainActivity;
import android.icu.text.IDNA;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import android.gasna.mission2ppe.R;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    View root;
    private HomeViewModel homeViewModel;

    private String departement;

    private String SERVER_URL = "http://192.168.0.37/Mission2PPEFinal/api.php?dep=75";

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        root = inflater.inflate(R.layout.fragment_home, container, false);

        Spinner spinDep = (Spinner) root.findViewById(R.id.spinnerDepartments);


        //Spinner Départements
        Spinner spinnerDepartements = (Spinner) root.findViewById(R.id.spinnerDepartments);
        ArrayAdapter<CharSequence> adapterDepartements = ArrayAdapter.createFromResource(getContext(), R.array.departements, android.R.layout.simple_spinner_item);
        adapterDepartements.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDepartements.setAdapter(adapterDepartements);
        spinnerDepartements.setOnItemSelectedListener(listenerDep);

        /**
        //Spinner Communes
        Spinner spinnerCommunes = (Spinner) root.findViewById(R.id.spinnerCommunes);
        ArrayAdapter<CharSequence> adapterCommunes = ArrayAdapter.createFromResource(getContext(), R.array.communes, android.R.layout.simple_spinner_item);
        adapterCommunes.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCommunes.setAdapter(adapterCommunes);
        spinnerCommunes.setOnItemSelectedListener(listenerCom);**/

        //Bouton
        Button button = (Button) root.findViewById(R.id.validate_button);
        button.setOnClickListener(listenerButton);

        spinDep.setOnItemSelectedListener(listenerDep);

        sendRequest(SERVER_URL);

        return root;
    }

    //Listener Spinner Départements
    private AdapterView.OnItemSelectedListener listenerDep = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            //TOAST et Log
            String message = "Département " + parent.getItemAtPosition(position).toString();
            Toast.makeText(parent.getContext(), message, Toast.LENGTH_SHORT).show();
            Log.i("departement", message);

            //Datas et Log
            departement = parent.getSelectedItem().toString();
            SERVER_URL =  SERVER_URL.substring(0,SERVER_URL.length() - 2) + departement;
            Log.i("url", SERVER_URL);
            sendRequest(SERVER_URL);

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    /**
    //Listener Spinner Communes
    private AdapterView.OnItemSelectedListener listenerCom = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String message = "Commune " + parent.getItemAtPosition(position).toString();
            Toast.makeText(parent.getContext(), message, Toast.LENGTH_SHORT).show();
            Log.i("commune", message);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };**/

    //Listener Bouton
    private View.OnClickListener listenerButton = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.validate_button:
                    v.setVisibility(View.INVISIBLE);

                    Fragment fragmentInterets = new InteretsFragment();

                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                    fragmentTransaction.replace(R.id.home_fragment, fragmentInterets);

                    fragmentTransaction.commit();
            }
        }
    };

    //Request URL
    private void sendRequest(final String SERVER_URL){
        StringRequest stringRequest = new StringRequest(SERVER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("reponse",""+response);
                        parseJSON(response);
                        Toast.makeText(getContext(),response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(),SERVER_URL+error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    //Parser le JSON
    private void parseJSON(String leJson){
        // Traitement de la source Json chargée dans onStart()
        Log.i("lejson",leJson.toString());
        ArrayList com = new ArrayList();
        try {
            JSONArray communes = (JSONArray) new JSONArray(leJson);

            Log.i("lejsoncommunes",communes.toString());
            // Pour chaque élément du tableau
            for (int i = 0; i < communes.length(); i++) {
                // Création d'un tableau élément à partir d'un JSONObject
                JSONObject jsonObj = new JSONObject(communes.getString(i));
                com.add(jsonObj.getString("commune"));
            }
            Log.i("data", com.toString());
        } catch (JSONException e) {
            Log.i("catch",e.toString());
            e.printStackTrace();
        }
        alimCommunes(com);
        Log.i("commune", "" + com);
    }

    private void alimCommunes(ArrayList com){
        Spinner spinCom = (Spinner) root.findViewById(R.id.spinnerCommunes);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, com);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinCom.setAdapter(adapter);
    }

}