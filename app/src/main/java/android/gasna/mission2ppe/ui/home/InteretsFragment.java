package android.gasna.mission2ppe.ui.home;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.content.res.AssetManager;
import android.gasna.mission2ppe.R;
import android.gasna.mission2ppe.ui.Interet;
import android.gasna.mission2ppe.ui.InteretAdapter;
import android.gasna.mission2ppe.ui.StatsInterets;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class InteretsFragment extends Fragment {

    private InteretsViewModel interetsViewModel;

    Gson gson = new Gson();
    ArrayList<Interet> interets = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        interetsViewModel =
                ViewModelProviders.of(this).get(InteretsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_interets, container, false);

        interets = buildInteretsList();
        ListView interetsList = (ListView) root.findViewById(R.id.interets_list);
        interetsList.setAdapter(new InteretAdapter(getActivity().getApplicationContext(), interets));

        interetsList.setOnItemClickListener(adaptaterListener);

        return root;
    }

    //Récupère la liste des intérets, extraite du json
    private ArrayList<Interet> buildInteretsList() {
        Type listType = new TypeToken<ArrayList<Interet>>() {
        }.getType();
        return gson.fromJson(readJSONFile("interets.json"), listType);
    }

    //Lis le JSON
    private String readJSONFile(String filename) {
        AssetManager am = getActivity().getAssets();
        String result = "";
        try {
            InputStream inputStream = am.open(filename);
            byte[] b = new byte[inputStream.available()];
            inputStream.read(b);
            result = new String(b);
        } catch (Exception e) {
            Log.e("Error readJSONFile", e.getMessage());
        }
        // Log.i("Result",result);
        return result;
    }

    //Adapter Listener Intérets Liste
    private AdapterView.OnItemClickListener adaptaterListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(getActivity().getApplication(), StatsInterets.class);
            intent.putExtra("elemPatri", interets.get(position).getElemPatri());
            intent.putExtra("elemPrinci", interets.get(position).getElemPrinci());
            startActivity(intent);
        }
    };

}
